#!/usr/bin/env python3

from base64 import encode
from collections import defaultdict
import json
from pathlib import Path
import yaml

rules = Path("rules.yaml")
with rules.open() as f:
    rules = yaml.load(f, Loader=yaml.SafeLoader)

# tag to text
tags_to_rules = defaultdict(list)
for r in rules:
    for t in r["tags"]:
        tags_to_rules[t].append(r)


# build markdown response for tag
for t, rules in tags_to_rules.items():
    # simply merge all the text together for this tag
    ## see if we can use multiple responses as described in:
    ## https://developers.mattermost.com/integrate/other-integrations/slash-commands/#parameters
    ## extra_responses
    mtm_texts = []
    signs = []
    for r in rules:
        mtm_texts.append(f"{r['text']} ({r['tags']})")
        ## append any sign only once !
        if "sign" in r and not r["sign"]["text"] in signs:
            signs.append(r["sign"]["text"])

    # all matching rules make a list
    text = "\n## Élèments correspondants\n"
    text += "\n* "
    text += "\n* ".join(mtm_texts)

    # append all the matching signs (if any)
    if signs:
        text += "\n## Signe(s) associé(s)\n"
        text += "\n---\n".join(signs)

    # NOTE(msimonin): for each sign we can find the inverse referecenced list of rules
    # This can be used to build the references sign 1->n rules
    # Not sure it's useful in the context of the mm bot but maybe nice for HTML rendering

    data = dict(
        text=text,
        # un-comment to spam your friends
        # response_type="in_channel",
    )
    # todo add all tags somewhere
    (Path("public") / f"{t}.json").write_text(json.dumps(data))
