# a naive Makefile to extract / create remote slash commands
.phony: extract slashes

all: extract slashes

extract:
	python parser.py

slashes:
	python slashes.py

clean:
	rm -rf public/*.json
