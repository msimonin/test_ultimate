import json
from pathlib import Path
import re

from lxml import etree
from markdownify import markdownify as md


BASE_URL = "https://mmouterde.github.io/regles-ultimate-fr"
MOTD = """## 🥏 Le point de règle du jour 🥏

fournit avec ❤️ par notre robot (_%s_)

---
"""

data = Path("index.html").read_text()
doc = etree.HTML(data)


def to_markdown(e):
    """Convert to markdown an HTML element.

    Some sanitization
    - remove one indent level
    - remove unecessary \n (otherwise some nested lists are rendered as code block)
    """
    _tag = e.tag
    e.tag = "div"
    text = md(etree.tostring(e))
    text = re.sub("\n+", "\n", text)
    e.tag = _tag
    return text


# modifie tous les liens pour qu'il pointent vers le site
# TODO(): faire plus fin
print("Fix links href")
links = doc.xpath("//a")
for link in links:
    href = link.attrib.get("href")
    if href:
        new_href = f"{BASE_URL}/{href}"
        print(f"{href} -> {new_href}")
        link.set("href", new_href)

# pareil pour les images
print("Fix img srcs")
imgs = doc.xpath("//img")
for img in imgs:
    src = img.attrib.get("src")
    if src:
        new_src = f"{BASE_URL}/{src}"
        print(f"{src} -> {new_src}")
        img.set("src", new_src)


# sélectionne que les élèments d'intérêts
# ceux dont on sait qu'ils forment une portion cohérente des règles
sections = doc.xpath("//li[@rules='1']")

for section in sections:
    if not "rules" in section.attrib:
        continue
    if not "tags" in section.attrib:
        continue

    # NOTE(msimonin): Should we automatically inherit from the ancestor tags ?
    tags = section.attrib["tags"].split(",")

    text = to_markdown(section)

    data = dict(
        text=text,
        # un-comment to spam your friends
        # response_type="in_channel",
    )
    for tag in tags:
        # todo add all tags somewhere
        (Path("public") / f"{tag}.json").write_text(json.dumps(data))

# sélectionne quelques faits de règles, ie sous ensemble cohérent (plus petit qu'au dessus)
facts = doc.xpath("//li[@fact='1']")
for idx, fact in enumerate(facts):
    fact_identifier = f"fact-{idx}"

    text = to_markdown(fact)
    text = f"""## 🥏 Le point de règle du jour 🥏

fournit avec ❤️ par notre robot (_{fact_identifier}_)

---
{text}

---
[en savoir plus]({BASE_URL})
"""

    data = dict(
        text=text,
        # un-comment to spam your friends
        # response_type="in_channel",
    )
    (Path("public") / f"{fact_identifier}.json").write_text(json.dumps(data))
